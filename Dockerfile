ARG TORCH_VERSION=latest

FROM tensorflow/tensorflow:2.15.0.post1-gpu-jupyter

USER root
RUN apt-get -qq -y update && \
    apt-get -qq -y upgrade && \
    apt-get -qq -y install \
        wget \
        curl \
        git \
        make \
        sudo

COPY requirements.txt .

RUN python3 -m pip install -r requirements.txt

EXPOSE 8888
